import os
import numpy as np
import matplotlib.pyplot as plt
import tensorflow as tf

n_classes = 2
image_width = 128
image_height = 128
batch_size = 16
capacity = 2000
iterator = 10000
learning_rate = 0.0001

###

def train():
    logs_path = 'checkpoint/'
    train, train_label = get_images('data/train/')
    train_batch, train_label_batch = get_batch(train, train_label, image_width, image_height, batch_size, capacity)
    train_model = cnn_model(train_batch, batch_size, n_classes)
    train_loss = losses(train_model, train_label_batch)        
    train_optimize = trainning(train_loss, learning_rate)
    train_accuracy = evaluation(train_model, train_label_batch)
    sum_optimize = tf.summary.merge_all()
    sess = tf.Session()
    writer = tf.summary.FileWriter(logs_path, sess.graph)
    saver = tf.train.Saver()
    sess.run(tf.global_variables_initializer())
    coord = tf.train.Coordinator()
    threads = tf.train.start_queue_runners(sess=sess, coord=coord)
    arr = np.array([], dtype=np.float32)
    for step in np.arange(iterator):
        if coord.should_stop():
                break
        optimize1, loss1, accuracy1 = sess.run([train_optimize, train_loss, train_accuracy])
           
        if step % 50 == 0:
            print('%d, loss: %.2f, accuracy: %.2f%%' %(step, loss1, accuracy1 * 100.0))
            sum_str = sess.run(sum_optimize)
            writer.add_summary(sum_str, step)
            arr = np.append(arr, loss1)
        
        if step % 2000 == 0 or (step + 1) == iterator:
            save_path = os.path.join(logs_path, 'ckpt')
            saver.save(sess, save_path, global_step=step)
    plt.ylabel('loss')
    plt.plot(arr)
    plt.show()
    coord.request_stop()
    coord.join(threads)
    sess.close()

###

def test():
    logs_path = 'checkpoint/'
    test, test_label = get_images('data/test/')
    with tf.Graph().as_default():    
        test_batch, test_label_batch = get_batch(test, test_label, image_width, image_height, batch_size,  capacity)
        test_model = cnn_model(test_batch, batch_size, n_classes)      
        test_accuracy = evaluation(test_model, test_label_batch)
        saver = tf.train.Saver()
        with tf.Session() as sess:
            ckpt = tf.train.get_checkpoint_state(logs_path)
            if ckpt and ckpt.model_checkpoint_path:
                saver.restore(sess, ckpt.model_checkpoint_path)
            else:
                print('Error')
            coord = tf.train.Coordinator()
            threads = tf.train.start_queue_runners(sess=sess, coord=coord)
            accuracy1 = sess.run(test_accuracy)
            print('accuracy: %.2f%%' %(accuracy1 * 100.0))
            coord.request_stop()
            coord.join(threads)
            sess.close()
###

def cnn_model(images, batch_size, n_classes):
    with tf.variable_scope('c1') as c1:
        weights = tf.get_variable('weights', shape=[5, 5, 3, 16], dtype=tf.float32, initializer=tf.truncated_normal_initializer(stddev=0.1,dtype=tf.float32))
        biases = tf.get_variable('biases', shape=[16], dtype=tf.float32, initializer=tf.constant_initializer(0.1))
        conv = tf.nn.conv2d(images, weights, strides=[1,1,1,1], padding='SAME')
        pre_activation = tf.nn.bias_add(conv, biases)
        conv1 = tf.nn.relu(pre_activation, name=c1.name)
    with tf.variable_scope('p1'):
        pool1 = tf.nn.max_pool(conv1, ksize=[1,3,3,1],strides=[1,2,2,1], padding='SAME', name='p1')
        norm1 = tf.nn.lrn(pool1, depth_radius=4, bias=1.0, alpha=0.001/9.0, beta=0.75,name='n1')
    with tf.variable_scope('c2'):
        weights = tf.get_variable('weights', shape=[5, 5, 16, 16], dtype=tf.float32, initializer=tf.truncated_normal_initializer(stddev=0.1,dtype=tf.float32))
        biases = tf.get_variable('biases', shape=[16], dtype=tf.float32, initializer=tf.constant_initializer(0.1))
        conv = tf.nn.conv2d(norm1, weights, strides=[1,1,1,1],padding='SAME')
        pre_activation = tf.nn.bias_add(conv, biases)
        conv2 = tf.nn.relu(pre_activation, name='c2')
    with tf.variable_scope('p2'):
        norm2 = tf.nn.lrn(conv2, depth_radius=4, bias=1.0, alpha=0.001/9.0, beta=0.75,name='n2')
        pool2 = tf.nn.max_pool(norm2, ksize=[1,3,3,1], strides=[1,1,1,1], padding='SAME',name='p2')
    with tf.variable_scope('fc1') as fc1:
        reshape = tf.reshape(pool2, shape=[batch_size, -1])
        dim = reshape.get_shape()[1].value
        weights = tf.get_variable('weights', shape=[dim,128], dtype=tf.float32, initializer=tf.truncated_normal_initializer(stddev=0.005,dtype=tf.float32))
        biases = tf.get_variable('biases', shape=[128], dtype=tf.float32, initializer=tf.constant_initializer(0.1))
        local3 = tf.nn.relu(tf.matmul(reshape, weights) + biases, name=fc1.name)
    with tf.variable_scope('fc2'):
        weights = tf.get_variable('weights', shape=[128,128], dtype=tf.float32, initializer=tf.truncated_normal_initializer(stddev=0.005,dtype=tf.float32))
        biases = tf.get_variable('biases', shape=[128], dtype=tf.float32, initializer=tf.constant_initializer(0.1))
        local4 = tf.nn.relu(tf.matmul(local3, weights) + biases, name='fc2')
    with tf.variable_scope('softmax'):
        weights = tf.get_variable('softmax_linear', shape=[128, n_classes], dtype=tf.float32, initializer=tf.truncated_normal_initializer(stddev=0.005,dtype=tf.float32))
        biases = tf.get_variable('biases', shape=[n_classes], dtype=tf.float32, initializer=tf.constant_initializer(0.1))
        softmax_linear = tf.add(tf.matmul(local4, weights), biases, name='softmax')
    return softmax_linear

def losses(logits, labels):
    with tf.variable_scope('loss') as scope:
        cross_entropy = tf.nn.sparse_softmax_cross_entropy_with_logits\
                        (logits=logits, labels=labels, name='xentropy_per_example')
        loss = tf.reduce_mean(cross_entropy, name='loss')
        tf.summary.scalar(scope.name+'/loss', loss)
    return loss

def trainning(loss, learning_rate):
    with tf.name_scope('optimizer'):
        optimizer = tf.train.AdamOptimizer(learning_rate= learning_rate)
        global_step = tf.Variable(0, name='global_step', trainable=False)
        train_optimize = optimizer.minimize(loss, global_step= global_step)
    return train_optimize

def evaluation(logits, labels):
  with tf.variable_scope('accuracy') as scope:
      correct = tf.nn.in_top_k(logits, labels, 1)
      correct = tf.cast(correct, tf.float16)
      accuracy = tf.reduce_mean(correct)
      tf.summary.scalar(scope.name+'/accuracy', accuracy)
  return accuracy

###

def get_images(path):
    c = []
    label_c = []
    d = []
    label_d = []
    for file in os.listdir(path):
        name = file.split('.')
        if name[0]=='cat':
            c.append(path + file)
            label_c.append(0)
        else:
            d.append(path + file)
            label_d.append(1)
    print('%d cats\n%d dogs' %(len(c), len(d)))
    images = np.hstack((c, d))
    labels = np.hstack((label_c, label_d))
    temp = np.array([images, labels])
    temp = temp.transpose()
    np.random.shuffle(temp)
    images = list(temp[:, 0])
    labels = list(temp[:, 1])
    labels = [int(i) for i in labels]
    return images, labels


def get_batch(image, label, width, height, batch_size, capacity):
    image = tf.cast(image, tf.string)
    label = tf.cast(label, tf.int32)
    queue = tf.train.slice_input_producer([image, label])
    label = queue[1]
    _image = tf.read_file(queue[0])
    image = tf.image.decode_jpeg(_image, channels=3)
    image = tf.image.resize_image_with_crop_or_pad(image, width, height)
    image = tf.image.per_image_standardization(image)
    image_batch, label_batch = tf.train.batch([image, label],
                                                batch_size= batch_size,
                                                num_threads= 64, 
                                                capacity = capacity)
    label_batch = tf.reshape(label_batch, [batch_size])
    image_batch = tf.cast(image_batch, tf.float32)
    return image_batch, label_batch

